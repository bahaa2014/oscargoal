package com.example.koora.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.example.koora.R;
import com.example.koora.uti.ToolsUtil;

import java.util.Calendar;
import java.util.Date;

public class BackgroundService extends Service {
    public BackgroundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Date currentTime = Calendar.getInstance().getTime();
        if (currentTime.getHours() == 21)
        {
            ToolsUtil.getTeam(getApplicationContext());
            startMessage(getApplicationContext(),"Match Between ");
        }
        return super.onStartCommand(intent,flags,startId);
    }
    int count = 0;
    public void startMessage(Context context,String description)
    {
        count++;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,"ch1");
        builder.setSmallIcon(R.drawable.logo);
        builder.setContentTitle("Match");
        builder.setContentText(description);
        builder.setVibrate(new long[]{1000,1000,1000});
        builder.setLights(Color.CYAN,3000,2000);

        Notification notification = builder.build();
        NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(count,notification);


    }
}
