package com.example.koora.ViewHolder;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.koora.R;

public class TeamsViewHolder  extends RecyclerView.ViewHolder{
    public ImageView img ;
    public TextView tvTeamName ;
    public TextView tvTeamCountry;
    public ConstraintLayout constraint;


    public TeamsViewHolder(@NonNull View itemView) {
        super(itemView);

        img =  itemView.findViewById(R.id.img);
        tvTeamName = itemView.findViewById(R.id.tvTeamName);
        tvTeamCountry = itemView.findViewById(R.id.tvTeamCountry);
        constraint = itemView.findViewById(R.id.constraint);


    }
}
