package com.example.koora.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.koora.Activites.DetailsTeam;
import com.example.koora.Models.Contests;
import com.example.koora.Models.Teams;
import com.example.koora.ViewHolder.TeamsViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ContestsAdapter extends  RecyclerView.Adapter<TeamsViewHolder>  {
    ArrayList<Contests> contests;
    int layoutId;
    Context context;

    public ContestsAdapter(ArrayList<Contests> contests, int layoutId, Context context) {
        this.contests = contests;
        this.layoutId = layoutId;
        this.context = context;
    }

    @NonNull
    @Override
    public TeamsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =   LayoutInflater.from(viewGroup.getContext()).inflate(layoutId,viewGroup,false);

        TeamsViewHolder vh = new TeamsViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TeamsViewHolder vh, final int position) {
        final Contests contest =  contests.get(position);
        vh.tvTeamName.setText(contest.getName());
        vh.tvTeamCountry.setText(contest.getCountry());
        vh.constraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, DetailsTeam.class);
                i.putExtra("contest_id",contests.get(position).getContest_id());
                i.putExtra("name",contests.get(position).getName());
                i.putExtra("country",contests.get(position).getCountry());

                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return contests.size();
    }
}
