package com.example.koora.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.koora.Activites.DetailsTeam;
import com.example.koora.Models.Teams;
import com.example.koora.ViewHolder.TeamsViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TeamsAdapter extends RecyclerView.Adapter<TeamsViewHolder> {
    ArrayList<Teams> teams;
    int layoutId;
    Context context;

    public TeamsAdapter(ArrayList<Teams> teams,int layoutId,Context context)
    {
        this.teams = teams;
        this.context = context;
        this.layoutId = layoutId;
    }

    @NonNull
    @Override
    public TeamsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =   LayoutInflater.from(viewGroup.getContext()).inflate(layoutId,viewGroup,false);

        TeamsViewHolder vh = new TeamsViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TeamsViewHolder vh, final int position) {
        final Teams team =  teams.get(position);
        Picasso.get().load(team.getImg()).into(vh.img);
        vh.tvTeamName.setText(team.getName());
        vh.tvTeamCountry.setText(team.getCountry());
        vh.constraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, DetailsTeam.class);
                i.putExtra("team_id",teams.get(position).getTeam_id());
                i.putExtra("name",teams.get(position).getName());
                i.putExtra("country",teams.get(position).getCountry());
                i.putExtra("img",teams.get(position).getImg());
                context.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return teams.size();
    }


}
