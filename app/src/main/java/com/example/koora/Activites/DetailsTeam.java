package com.example.koora.Activites;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.koora.DB.DB_Helper;
import com.example.koora.R;
import com.squareup.picasso.Picasso;


public class
DetailsTeam extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_team);
        DB_Helper db_helper = new DB_Helper(getApplicationContext(),"oscar_db",1);
        Intent i = getIntent();
        String team_id = i.getStringExtra("team_id");
        String team_name =  i.getStringExtra("name");
        String team_country =  i.getStringExtra("country");
        String img_url =  i.getStringExtra("img");

        ImageView ivLogo = findViewById(R.id.logo);
        TextView tvTeamName = findViewById(R.id.tvTeamName);
        TextView tvTeamCountry = findViewById(R.id.tvTeamCountry);
        Button btnAccept = findViewById(R.id.btnAccept);

        Picasso.get().load(img_url).into(ivLogo);
        tvTeamName.setText(team_name);
        tvTeamCountry.setText(team_country);

        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor =  db.rawQuery("select * from Favorite",null);


        cursor.moveToFirst();

        while (!cursor.isAfterLast())
        {

            Integer id = cursor.getColumnIndex("fav_id");
            String fav_id = cursor.getString(id);

            Integer type = cursor.getColumnIndex("fav_type");
            String fav_type = cursor.getString(type);
            if (fav_type.equals("team"))
            {
                if (team_id == fav_id)
                {
                    btnAccept.setText("Delete Favorite");

                }
            }

        }

        cursor.close();
        db.close();





        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }
}
