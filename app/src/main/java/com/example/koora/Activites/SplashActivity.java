package com.example.koora.Activites;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.koora.DB.DB_Helper;
import com.example.koora.R;
import com.example.koora.uti.ToolsUtil;

public class SplashActivity extends AppCompatActivity {
    ImageView ivSplash;
    TextView tvAppName,tvAppDescr;
    Button btnStart;
    Animation resize,right_to_left;
    boolean haveTeam = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ReadContest();
        ReadTeams();


    }
    private void ReadTeams()
    {
        DB_Helper db_helper = new DB_Helper(getApplicationContext(),"oscar_db",1);
        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor =  db.rawQuery("select * from Teams",null);


        cursor.moveToFirst();

        while (!cursor.isAfterLast())
        {

            haveTeam = true;
            break;

        }

        cursor.close();
        if (!haveTeam)
        {

            ToolsUtil.getTeam(getApplicationContext());
        }


        db.close();
    }
    private void ReadContest()
    {
        DB_Helper db_helper = new DB_Helper(getApplicationContext(),"oscar_db",1);
        SQLiteDatabase db = db_helper.getReadableDatabase();
        Cursor cursor_contest =  db.rawQuery("select * from Contests",null);


        cursor_contest.moveToFirst();

        while (!cursor_contest.isAfterLast())
        {

            haveTeam = true;
            break;

        }

        cursor_contest.close();
        if (!haveTeam)
        {

            ToolsUtil.getContest(getApplicationContext());
        }
        db.close();

        StartAnimation();

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });
    }
    private void StartAnimation()
    {
        resize = AnimationUtils.loadAnimation(this,R.anim.resize);
        right_to_left = AnimationUtils.loadAnimation(this,R.anim.right_to_left);

        tvAppName = (TextView)findViewById(R.id.tvAppName);
        tvAppDescr = (TextView)findViewById(R.id.tvAppDescr);
        btnStart = (Button)findViewById(R.id.btnStart);
        ivSplash = (ImageView)findViewById(R.id.ivLogo);

        ivSplash.startAnimation(resize);
        tvAppName.startAnimation(right_to_left);
        tvAppDescr.startAnimation(right_to_left);
        btnStart.startAnimation(right_to_left);
    }
}
