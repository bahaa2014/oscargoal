package com.example.koora.Activites;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.koora.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Login extends AppCompatActivity {
    Button login;
    FirebaseAuth auth;
    EditText etEmailLogin, etPasswordaLogin, confirmPassword;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(R.string.nav_login);

        }else {

            ab.setDisplayHomeAsUpEnabled(true);
        }


        auth = FirebaseAuth.getInstance();
        try {
            FirebaseUser user = auth.getCurrentUser();
            if (!user.getEmail().isEmpty()){
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }

        }catch (Exception e){
            Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
//        UpdatUI(user);

        login = findViewById(R.id.btnLogin);
        etEmailLogin = findViewById(R.id.etEmailLogin);
        etPasswordaLogin = findViewById(R.id.etPasswordaLogin);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait....");
        dialog.setCancelable(false);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmailLogin.getText().toString();
                String password = etPasswordaLogin.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Please insert Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Please insert Password", Toast.LENGTH_SHORT).show();
                }
                dialog.show();
                Task<AuthResult> task = auth.signInWithEmailAndPassword(email, password);
                task.addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;

                        Toast.makeText(getApplicationContext(), "" + currentFirebaseUser.getUid(), Toast.LENGTH_SHORT).show();

                        Log.d("task2",task.getResult()+"");
                        Log.d("id::",currentFirebaseUser.getUid()+"");


                        dialog.dismiss();
                        if (task.isSuccessful()) {
                            FirebaseUser user = auth.getCurrentUser();

//                            UpdatUI(user);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "Email or Password Error", Toast.LENGTH_SHORT).show();
                        }




                    }
                });

                task.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
//                        txtLogin.setText(e.getLocalizedMessage());

                    }
                });

            }
        });
    }


    private void UpdatUI(FirebaseUser user){
        if (user != null) {
            String userNamea = user.getEmail();
//            txtLogin.setText(userNamea);
//            txtLogin.setTextColor(Color.GREEN);
        }

    }


}
