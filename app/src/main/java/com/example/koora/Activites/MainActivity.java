package com.example.koora.Activites;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.koora.Activites.AboutActivity;
import com.example.koora.DB.DB_Helper;
import com.example.koora.Fragments.ContestsFragment;
import com.example.koora.Fragments.HomeFragment;
import com.example.koora.Fragments.TeamsFragment;
import com.example.koora.R;
import com.example.koora.Service.BackgroundService;
import com.example.koora.uti.ToolsUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import android.support.v7.app.ActionBar;

public class MainActivity extends AppCompatActivity {



    private Toolbar toolbar;

    FirebaseUser user;
    FirebaseAuth auth;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setTitle(R.string.nav_team);
                    fragment = new TeamsFragment();
                    loadFragment(new TeamsFragment());

                    return true;
                case R.id.navigation_dashboard:
                    setTitle(R.string.nav_Periodicals);
                    fragment = new ContestsFragment();
                    loadFragment(new ContestsFragment());
                    return true;
                case R.id.navigation_notifications:
                    setTitle(R.string.nav_fav);
                    fragment = new HomeFragment();
                    return true;
                case R.id.navigation_notificationss:
                    setTitle(R.string.nav_about);
                    if (user != null) {
                        FirebaseAuth.getInstance().signOut();
                        finish();
                        startActivity(new Intent(MainActivity.this, Login.class));
                    }
                    fragment = new HomeFragment();
                    return true;
                case R.id.navigation_notificationsa:
                    setTitle(R.string.nav_login);
                    startActivity(new Intent(MainActivity.this, Login.class));

                    return true;
            }
            return false;
        }
    };
    private void createChannel()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel("ch1","OscarGoal", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("This Channel For Notification early night");
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM),null);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000,1000,1000});
            channel.enableLights(true);
            channel.setLightColor(Color.CYAN);
            NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(channel);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    createChannel();
        startService(new Intent(this, BackgroundService.class));

        BottomNavigationView navView = findViewById(R.id.nav_view);
//        mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadFragment(new TeamsFragment());


        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.nav_team);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.nav_logout) {
            if (user != null) {
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this, Login.class));
            }
        }
        return true;
    }
    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

}
