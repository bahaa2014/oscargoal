package com.example.koora.Models;

public class Contests {
    public String contest_id;
    public String name;
    public String country;

    public Contests()
    {

    }
    public Contests(String contest_id, String name, String country_id) {
        this.contest_id = contest_id;
        this.name = name;
        this.country = country_id;
    }

    public String getContest_id() {
        return contest_id;
    }

    public void setContest_id(String contest_id) {
        this.contest_id = contest_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
