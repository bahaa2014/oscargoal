package com.example.koora.Models;

public class Teams {
    private String team_id;
    private String name;
    private String country;
    private String img;

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public Teams()
    {

    }
    public Teams(String name, String country, String img) {
        this.name = name;
        this.country = country;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
