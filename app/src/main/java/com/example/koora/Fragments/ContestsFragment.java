package com.example.koora.Fragments;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.koora.Adapters.ContestsAdapter;
import com.example.koora.Adapters.TeamsAdapter;
import com.example.koora.DB.DB_Helper;
import com.example.koora.Models.Contests;
import com.example.koora.Models.Teams;
import com.example.koora.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContestsFragment extends Fragment {

    RecyclerView recyclerView_team;
    ArrayList<Contests> contests= new ArrayList<>();

    public ContestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teams, container, false);
        recyclerView_team=view.findViewById(R.id.recyclerView_team);

        Toast.makeText(getContext(), "opened contest", Toast.LENGTH_SHORT).show();



        DB_Helper db_helper = new DB_Helper(getContext(),"oscar_db",1);
        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor =  db.rawQuery("select * from Contests",null);


        cursor.moveToFirst();

        while (!cursor.isAfterLast())
        {

            Integer id = cursor.getColumnIndex("contest_id");
            String contest_id = cursor.getString(id);

            Integer name = cursor.getColumnIndex("name");
            String contest_name = cursor.getString(name);

            Integer country = cursor.getColumnIndex("country_name");
            String country_name = cursor.getString(country);


            Contests contest = new Contests();
            contest.setName(contest_name);
            contest.setCountry(country_name);
            contest.setContest_id(contest_id);


            contests.add(contest);



            cursor.moveToNext();

        }


        cursor.close();

        RecyclerView rv = (RecyclerView)view.findViewById(R.id.recyclerView_team);
        ContestsAdapter personAdapter = new ContestsAdapter(contests,R.layout.cust_item_clubs,getContext());
        rv.setAdapter(personAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(linearLayoutManager);

        DividerItemDecoration decoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rv.addItemDecoration(decoration);




        return view;

    }

}
