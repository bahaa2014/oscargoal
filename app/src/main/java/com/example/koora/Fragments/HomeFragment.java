package com.example.koora.Fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.koora.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private static ViewPager mPager;
    private TabLayout mTabLayout;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
//        mPager = (ViewPager) view.findViewById(R.id.pager);
//        mTabLayout =  view.findViewById(R.id.tab_layout);

//        mPager.setAdapter(new TabsAdapter(getChildFragmentManager()));
//        mTabLayout.setupWithViewPager(mPager);
//
        setHasOptionsMenu(true);

        return view;
    }

//    class TabsAdapter extends FragmentPagerAdapter {
//
//        public TabsAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public int getCount() {
//            return 5;
//        }
//
//        @Override
//        public Fragment getItem(int i) {
//            switch (i) {
//                case 0:
//                    return new TabGeometry();
//                case 1:
//                    return new TabGeoCone();
//                case 2:
//                    return new TabGeoCone();
//                case 3:
//                    return new TabGeoCone();
//                case 4:
//                    return new TabGeoCone();
//            }
//            return null;
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            switch (position) {
//                case 0:
//                    return "Cube";
//                case 1:
//                    return "Rectangular";
//                case 2:
//                    return "Cylinder";
//                case 3:
//                    return "Cone";
//                case 4:
//                    return "Sphere";
//            }
//            return "";
//        }
//    }
}
