package com.example.koora.Fragments;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.koora.Adapters.TeamsAdapter;
import com.example.koora.DB.DB_Helper;
import com.example.koora.Models.Teams;
import com.example.koora.R;
import com.example.koora.app.AppController;
import com.example.koora.uti.ToolsUtil;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeamsFragment extends Fragment {

    RecyclerView recyclerView_team;
    ArrayList<Teams> teams= new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teams, container, false);
        recyclerView_team=view.findViewById(R.id.recyclerView_team);

//        for (int ix = 0 ; ix < 15 ; ix++)
//        {
//            Teams t1 = new Teams();
//            t1.setTeam_id(""+ix);
//            t1.setName("Barcalona "+ix);
//            t1.setCountry("Ispan");
//            t1.setImg("https://www.footballexperience.com/image/cache/catalog/New-Logo/LA%20LIGA%20TEAMS/fc-barcelona%20ruuu_opt-250x250.png");
//            teams.add(t1);
//        }

        DB_Helper db_helper = new DB_Helper(getContext(),"oscar_db",1);
        SQLiteDatabase db = db_helper.getReadableDatabase();

        Cursor cursor =  db.rawQuery("select * from Teams",null);


        cursor.moveToFirst();

        while (!cursor.isAfterLast())
        {

            Integer id = cursor.getColumnIndex("team_id");
            String team_id = cursor.getString(id);

            Integer name = cursor.getColumnIndex("name");
            String team_name = cursor.getString(name);

            Integer country = cursor.getColumnIndex("country_name");
            String country_name = cursor.getString(country);

            Integer logo = cursor.getColumnIndex("logo");
            String logo_url = cursor.getString(logo);

            Teams team = new Teams();
            team.setName(team_name);
            team.setCountry(country_name);
            team.setTeam_id(team_id);
            team.setImg(logo_url);

            teams.add(team);



            cursor.moveToNext();

        }


        cursor.close();

              RecyclerView rv = (RecyclerView)view.findViewById(R.id.recyclerView_team);
              TeamsAdapter personAdapter = new TeamsAdapter(teams,R.layout.cust_item_clubs,getContext());
              rv.setAdapter(personAdapter);

              LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
              linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
              rv.setLayoutManager(linearLayoutManager);

              DividerItemDecoration decoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
              rv.addItemDecoration(decoration);




        return view;

    }

//    public void getTeam(final View view) {
//
//        ToolsUtil.ROOT_URL_Sub = "teams";
//
//        JsonObjectRequest  request = new JsonObjectRequest(Request.Method.GET, ToolsUtil.ROOT_URL + ToolsUtil.ROOT_URL_Sub,null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                try {
//
//                    JSONArray data = response.getJSONArray("data");
//
//                    for (int index =0;index < data.length();index++)
//                    {
//                        JSONObject result = data.getJSONObject(index);
//
//                        if (data != null) {
//
//                            String team_id = result.getString("id");
//                            String team_name = result.getString("name");
//                            JSONObject country = result.getJSONObject("countries");
//                            String country_name = country.getString("name");
//                            String image_url = result.getString("logo");
//
//
//
//
//                            if (team_name.length() > 0)
//                            {
//
//                                Teams t1 = new Teams();
//                                t1.setTeam_id(team_id);
//                                t1.setName(team_name);
//                                t1.setCountry(country_name);
//                                t1.setImg(image_url);
//
//                                teams.add(t1);
//                            }
//
//                        }
//                        else {
//                            Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                    RecyclerView rv = (RecyclerView)view.findViewById(R.id.recyclerView_team);
//                    TeamsAdapter personAdapter = new TeamsAdapter(teams,R.layout.cust_item_clubs,getContext());
//                    rv.setAdapter(personAdapter);
//
//                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
//                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//                    rv.setLayoutManager(linearLayoutManager);
//
//                    DividerItemDecoration decoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
//                    rv.addItemDecoration(decoration);
//
//
//
//                } catch (JSONException e) {
//                    //e.printStackTrace();
//
//
//                }
//
//
//
//            }
//
//
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), "error "+error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        AppController.getInstance().getRequestQueue().add(request);
//
//
//    }


}
