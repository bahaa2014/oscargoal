package com.example.koora.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DB_Helper extends SQLiteOpenHelper {
    public DB_Helper(Context context, String name,  int version) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Teams (id integer primary key autoincrement not null,team_id integer not null,name text not null,country_name text not null,logo text not null)");
        db.execSQL("create table Contests (id integer primary key autoincrement not null,contest_id integer not null,name text not null,country_name text not null)");
        db.execSQL("create table Favorite (id integer primary key autoincrement not null,fav_id integer not null,fav_name text not null,fav_type text not null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists Teams");
        db.execSQL("drop table if exists Contests");
        db.execSQL("drop table if exists Favorite");
        onCreate(db);
    }
}
