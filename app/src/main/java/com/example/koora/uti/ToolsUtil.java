package com.example.koora.uti;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.koora.Adapters.TeamsAdapter;
import com.example.koora.DB.DB_Helper;
import com.example.koora.Models.Contests;
import com.example.koora.Models.Teams;
import com.example.koora.R;
import com.example.koora.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.util.ArrayList;

public class ToolsUtil {

    public static String ROOT_URL = "http://192.168.137.235/OscarGoal/public/api/";
    public static String ROOT_URL_Sub = "teams";


    public static boolean IsReadTeam = false;

     public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }

    public static void getTeam(final Context context) {

        ToolsUtil.ROOT_URL_Sub = "teams";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, ToolsUtil.ROOT_URL + ToolsUtil.ROOT_URL_Sub,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray data = response.getJSONArray("data");
                     DB_Helper db_helper = new DB_Helper(context,"oscar_db",1);
                    for (int index =0;index < data.length();index++)
                    {
                        JSONObject result = data.getJSONObject(index);

                        if (data != null) {

                            String team_id = result.getString("id");
                            String team_name = result.getString("name");
                            JSONObject country = result.getJSONObject("countries");
                            String country_name = country.getString("name");
                            String image_url = result.getString("logo");




                            if (team_name.length() > 0)
                            {

                                Teams t1 = new Teams();
                                t1.setTeam_id(team_id);
                                t1.setName(team_name);
                                t1.setCountry(country_name);
                                t1.setImg(image_url);



                                SQLiteDatabase db = db_helper.getWritableDatabase();
                                ContentValues cv = new ContentValues();
                                cv.put("team_id",team_id);
                                cv.put("name",team_name);
                                cv.put("country_name",country_name);
                                cv.put("logo",image_url);
                                long result_db = db.insert("Teams",null,cv);

                                db.close();
                            }

                        }
                        else {
                            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }




                } catch (JSONException e) {
                    //e.printStackTrace();

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }



            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "error "+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().getRequestQueue().add(request);


    }

    public static void getContest(final Context context) {

        ToolsUtil.ROOT_URL_Sub = "contests";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, ToolsUtil.ROOT_URL + ToolsUtil.ROOT_URL_Sub,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray data = response.getJSONArray("data");
                    DB_Helper db_helper = new DB_Helper(context,"oscar_db",1);
                    for (int index =0;index < data.length();index++)
                    {
                        JSONObject result = data.getJSONObject(index);

                        if (data != null) {

                            String contest_id = result.getString("id");
                            String contest_name = result.getString("name");
                            JSONObject country = result.getJSONObject("countries");
                            String country_name = country.getString("name");




                            if (contest_name.length() > 0)
                            {

                                Contests c1 = new Contests();
                                c1.setContest_id(contest_id);
                                c1.setName(contest_name);
                                c1.setCountry(country_name);



                                SQLiteDatabase db = db_helper.getWritableDatabase();
                                ContentValues cv = new ContentValues();
                                cv.put("contest_id",contest_id);
                                cv.put("name",contest_name);
                                cv.put("country_name",country_name);
                                long result_db = db.insert("Contests",null,cv);

                                db.close();
                            }

                        }
                        else {
                            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }




                } catch (JSONException e) {
                    //e.printStackTrace();

                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }



            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "error "+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().getRequestQueue().add(request);


    }
}
